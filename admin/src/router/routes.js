
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: () => import('pages/Index.vue'),
      },
      {
        path: '/users',
        name: 'Users',
        component: () => import('pages/users/users.vue'),
      },
      {
        path: '/roles',
        name: 'Roles',
        component: () => import('pages/roles/roles.vue'),
      },
      {
        path: '/group',
        name: 'Groups',
        component: () => import('pages/group/group.vue'),
      },
      {
        path: '/subject',
        name: 'Subjects',
        component: () => import('pages/subject/subject.vue'),
      },
      {
        path: '/lesson',
        name: 'Lessons',
        component: () => import('pages/lesson/lesson.vue'),
      },

    ]
  },
  {
    path: '/logout',
    component: () => import('layouts/Login.vue'),
  },
  {
    path: '/login',
    component: () => import('layouts/Login.vue'),
  },
  {
    path: '/task',
    name: 'Task',
    component: () => import('pages/task/task.vue'),
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
