import { boot } from 'quasar/wrappers'
import { Notify } from 'quasar'
import axios from 'axios'
const domen= ()=>{
  return "http://127.0.0.1:222/api/";
}
const e = (mess) => {
    Notify.create({
        type: 'negative',
        message: mess,
        position: "bottom-right",
        progress: true,
        actions: [
            { icon: 'close', size: "sm", color: 'white', handler: () => { /* ... */ } }
        ]
    })
}

const s = (mess) => {
    Notify.create({
        type: 'positive',
        message: mess,
        position: "bottom-right",
        progress: true,
        actions: [
            { icon: 'close', size: "sm", color: 'white', handler: () => { /* ... */ } }
        ]
    })
}

const w = (mess) => {
    Notify.create({
        type: 'warning',
        message: mess,
        position: "bottom-right",
        progress: true,
        actions: [
            { icon: 'close', size: "sm", color: 'white', handler: () => { /* ... */ } }
        ]
    })
}

const i = (mess) => {
    Notify.create({
        type: 'info',
        message: mess,
        position: "bottom-right",
        progress: true,
        actions: [
            { icon: 'close', size: "sm", color: 'white', handler: () => { /* ... */ } }
        ]
    })
}

const isPerCrud = (route, event, data1) => {
            var data = JSON.parse(data1)
            ///console.log(data)
            if(data == null) return false
            for(var i=0;i<data.length;i++){
                //console.log(data[i].url +" "+ route)
                if(route == data[i].url){
                    //console.log(data[i][event])
                    if(data[i][event]){
                        //console.log(data)
                        return true
                    }else{
                        return false
                    }
                }
            }
            return false
}

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async({ app, router, Vue }) => {
    app.config.globalProperties.$e = e
    app.config.globalProperties.$s = s
    app.config.globalProperties.$w = w
    app.config.globalProperties.$i = i
    app.config.globalProperties.$isPerCrud = isPerCrud
    app.config.globalProperties.$domen = domen
})
