export default function () {
  return {
    user: null,
    userPermission: null,
    stateResources: [
      {name: 'Dashboard',title: 'Dashboard', url: '/', iconUrl: "inbox", read:false, write: false, update: false, delete: false,input: null, output: null},
      // {name: 'Store',title: 'Store', url: '/store', iconUrl: "home", read:false, write: false, update: false, delete: false,input: null, output: null},
      // {name: 'Category',title: 'Category', url: '/category', iconUrl: "category", read:false, write: false, update: false, delete: false,input: null, output: null},
      // {name: 'Product',title: 'Product', url: '/product', iconUrl: "shopping_cart", read:false, write: false, update: false, delete: false,input: null, output: null},
      // {name: 'Statistic',title: 'Statistic', url: '/statistic', iconUrl: "leaderboard", read:false,write: null, update: null, delete: null, input: false, output: false},
      {name: 'Users',title: 'Users', url: '/users', iconUrl: "person", read:false, write: false, update: false, delete: false,input: null, output: null},
      {name: 'Roles',title: 'Roles', url: '/roles', iconUrl: "settings", read:false, write: false, update: false, delete: false,input: null, output: null},
      // {name: 'Suppliers',title: 'Suppliers', url: '/supplier', iconUrl: "settings", read:false, write: false, update: false, delete: false,input: null, output: null}
    ],
  }
}