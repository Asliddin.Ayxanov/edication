import state from "./state"

export function setUser(state, data) {
    state.user = data 
    state.userPermission = JSON.parse(data.user.role.permission)
}

export function DelUser(state) {
    state.user = null 
    state.userPermission = []
}