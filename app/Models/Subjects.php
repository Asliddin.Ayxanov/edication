<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    protected $fillable = ['id', 'name'];

    public function group()
    {
        return $this->belongsToMany('App\Models\Groups', 'subject_groups','id','subject_id');
    }
    public function lesson(){
        return $this->hasMany("App\Models\Lessons",'subject_id');
    }
}
