<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Javob extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'user_id', 'test_id', 'javob'];
    public function user()
    {
        return $this->belongsToMany('App\Models\User', 'group_lessons','id','user_id');
    }
}
