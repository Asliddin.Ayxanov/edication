<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{

    protected $fillable = ['id', 'name'];
    public function user()
    {
        return $this->belongsToMany('App\Models\User', 'group_users','id','group_id');
    }
    public function subject()
    {
        return $this->belongsToMany('App\Models\Subjects', 'subject_groups','id','group_id');
    }
}
