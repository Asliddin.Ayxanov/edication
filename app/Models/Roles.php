<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $fillable = ["id", "name","permission"];

    public function user(){
        return $this->hasMany("App\User");
    }
}
