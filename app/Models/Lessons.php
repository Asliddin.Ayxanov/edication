<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lessons extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'name', 'time','subject_id'];
    public function suject()
    {
        return $this->belongsTo('App\Models\Subjects','subject_id');
    }

}
