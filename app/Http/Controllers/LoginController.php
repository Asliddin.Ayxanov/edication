<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;


class LoginController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate(true);
            $user = User::where("id", Auth::user()->id)->with('role')->get();
            // $token=User::find(Auth::user()->id)->createToken('auth_token')->plainTextToken;
            // $data=[
            //     'user'=>$user,
            //     'token'=>$token
            // ];
            return $user;
        }
        return
            response()->json([
                'msg'=> "You are not authorized"
            ], 401);
    }
}
