<?php

namespace App\Http\Controllers;
use App\Models\Lessons;
use App\Models\Group_User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LessonController extends Controller
{
    public function getLesson(Request $request,$id)
    {
        $Lessons = Lessons::where('subject_id',$id)->orderBy('id')->get();
        return $Lessons;
    }
    public function Createlesson(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'time' => 'required',
        ]);
        $data = [
            'name' => $request->name,
            'subject_id' => $id,
            'time' => $request->time,
        ];

        $Lessons = Lessons::create($data);
        return $Lessons;
    }

    public function DeleteLesson(Request $request,$id)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        return Lessons::where('id', $request->id)->delete();
    }

    public function UpdateLesson(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required|min:3',
            'time' => 'required',
        ]);
        $data=[
            'name' => $request->name,
            'time' => $request->time,
        ];
        $group = Lessons::where('id', $request->id)->update($data);
        return $group;
    }
    public function multidelete(Request $request){
        $data = json_decode($request->data);
        $rus=[];
        foreach ($data as $val) {
            // return $val->id;
            
            
            $rus[]= Lessons::find($val->id)->delete();
        }
        return $rus;
    }
}
