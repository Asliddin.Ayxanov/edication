<?php

namespace App\Http\Controllers;
use App\Models\Subjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubjectController extends Controller
{
    public function getSubject(Request $request)
    {
        $Subjects = Subjects::orderBy('id')->with('lesson')->get();
        return $Subjects;
    }
    
    public function CreateSubject(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
        ]);
        $Subject = Subjects::create([
            'name' => $request->name,
            'user_id'=>Auth::user()->id
        ]);
        return $Subject;
    }
    public function DeleteSubject(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        return Subjects::where('id', $request->id)->delete();
    }

    public function UpdateSubject(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
        ]);
        $data=[
            'name'=>$request->name
        ];  
        $Subject = Subjects::where('id', $request->id)->update($data);
        return $Subject;
    }
    public function multidelete(Request $request){
        $data = json_decode($request->data);
        $rus=[];
        foreach ($data as $val) {
            // return $val->id;
            
            
            $rus[]= Subjects::find($val->id)->delete();
        }
        return $rus;
    }
}
