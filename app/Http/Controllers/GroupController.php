<?php

namespace App\Http\Controllers;
use App\Models\Groups;
use App\Models\Group_User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public function getGroup(Request $request)
    {
        $Groups = Groups::orderBy('id')->get();
        return $Groups;
    }
    
    public function CreateGroup(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
        ]);
        $data = [
            'name' => $request->name,
        ];

        $group = Groups::create([
            'name' => $request->name,
            
        ]);
        Group_User::create([
            'user_id'=>Auth::user()->id??1,
            'group_id'=>$group->id
        ]);
        return $group;
    }
    public function DeleteGroup(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        return Groups::where('id', $request->id)->delete();
    }

    public function UpdateGroup(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
        ]);
        $data=[
            'name'=>$request->name
        ];  
        $group = Groups::where('id', $request->id)->update($data);
        return $group;
    }
    public function multidelete(Request $request){
        $data = json_decode($request->data);
        $rus=[];
        foreach ($data as $val) {
            // return $val->id;
            
            
            $rus[]= Groups::find($val->id)->delete();
        }
        return $rus;
    }
}
