<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function AuthUser(){
        // return User::where("id", 1)->with('role')->get();
        if(isset(Auth::user()->id)){
            $user = User::where("id", Auth::user()->id)->with('role')->get();
            return $user;
        }
        return response()->json([
            'msg'=> "You are not authorized"
        ], 401); 
    }
    public function Logout(){
        Auth::logout();
        return response()->json([
            'msg'=>'Success'
        ], 200);
    }
    public function GetUsers(Request $request){
        $dt = User::orderBy("id")->with('role')->get();
        return $dt;
    }

    public function CreateUser(Request $request){
        $this->validate($request, [
            "name" => "required|min:3",
            "maktab" => "required",
            "sinf" => "required",
            "email" => "bail|required|unique:users",
            "password" => "bail|required|min:3",
            "role_id" => "required"
        ]);
        // return $request;
        $data = [
            "name" => $request->name,
            "email" => $request->email,
            "maktab" => $request->maktab,
            "sinf" => $request->sinf,
            "role_id"  => $request->role_id
        ];
        // return $data;
        $p = $request->password;
        $p_r = $request->return_password;
        if($p != "" || $p_r != ""){
            if($p != $p_r){
                return response()->json([
                    'msg'=> "You entered two password differently"
                ], 422);        
            }
            $password = bcrypt($request->password);
            $data["password"] = $password;
        } 
        
        $user = User::create($data);
        return $user;
    }


    public function UpdateUser(Request $request){
      
        $this->validate($request, [
            "id" => "required",
            "name" => "required|min:3",
            "phone" => "bail|required|email|unique:users,phone,$request->id",
            "maktab" => "required",
            "sinf" => "required",
            "password" => "bail|required|min:3",
            "role_id" => "required"
        ]);
        
        $data = [
            "name" => $request->name,
            "email" => $request->phone,
            "maktab" => $request->maktab,
            "sinf" => $request->sinf,
            "role_id"  => $request->role_id
        ];
        $p = $request->password;
        $p_r = $request->return_password;
        if($p != "" || $p_r != ""){
            if($p != $p_r){
                return response()->json([
                    'msg'=> "You entered two password differently"
                ], 422);        
            }
            $password = bcrypt($request->password);
            $data["password"] = $password;
        }        
        $user = User::where("id", $request->id)->update($data);
        return $user;
    }

    public function DeleteUser(Request $request){
        $this->validate($request, [
            "id"=>"required",
            "phone" => "bail|required|unique:users,phone,$request->id",            
        ]);
        return User::where("phone", $request->phone)->delete();
    }

    public function DeleteUser1($request){
        if(!isset($request->id)){return "id not found";}
        if(!isset($request->email)){return "email not found";}
        return User::where("email", $request->email)->delete();
    }

    public function MoreDeleteUser(Request $request){
        $res = [];
        $i=0;
        while(isset($request[$i])){
            $json = [
                'id'=>$request[$i]['id'],
                'email'=>$request[$i]['email']
            ];
            $res[] = $this->DeleteUser1(json_decode(json_encode($json)));
            $i++;
        }
        return response()->json([
            'data'=> $res
        ], 200); 
    }
}