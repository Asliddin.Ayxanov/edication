<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Roles;
class RolesController extends Controller
{
    public function GetRoles(Request $request){
        return Roles::orderBy("id")->get();
    }

    public function CreateRole(Request $request){
        $this->validate($request, [
            "name" => "required|min:3",
            "permission"=>"required",
        ]);

        return Roles::create([
            "name" => $request->name,
            "permission" => $request->permission
        ]);
    }

    public function UpdateRole(Request $request){
        $this->validate($request, [
            "id"=>"required",
            "name"=>"required|min:3",
            "permission" => "required|min:3"
        ]);

        return Roles::where("id", $request->id)->update([
            "name"=>$request->name,
            "permission" => $request->permission
        ]);
    }

    public function DeleteRole(Request $request){
        $this->validate($request, [
            "id"=>"required",
            "name"=>"required|min:3",
            "permission" => "required|min:3"
        ]);

        return Roles::where("id", $request->id)->delete();
    }
}
