<?php

namespace Database\Seeders;
use Illuminate\Support\Carbon;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = '[
            {
                "name":"Dashboard",
                "title":"Dashboard",
                "url":"/",
                "iconUrl":"inbox",
                "read":true,
                "write":true,
                "update":true,
                "delete":true,
                "input":null,
                "output":null
            },
            {
                "name":"Groups",
                "title":"Groups",
                "url":"/group",
                "iconUrl":"group",
                "read":true,
                "write":true,
                "update":true,
                "delete":true,
                "input":null,
                "output":null
            },
            {
                "name":"Fanlar",
                "title":"Fanlar",
                "url":"/subject",
                "iconUrl":"subject",
                "read":true,
                "write":true,
                "update":true,
                "delete":true,
                "input":null,
                "output":null
            },
            {"name":"Users","title":"Users","url":"/users","iconUrl":"person","read":true,"write":true,"update":true,"delete":true,"input":null,"output":null},{"name":"Roles","title":"Roles","url":"/roles","iconUrl":"settings","read":true,"write":true,"update":true,"delete":true,"input":null,"output":null}]';
        DB::table("roles")->insert(
            [
                "name" => "Super Admin",
                "permission" => $permission,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],    
        );
        DB::table("users")->insert(
            [
                "name" => "Asliddin",
                "email" => "915957720",
                "maktab" => "54",
                "sinf" => "11",
                "password" => bcrypt("12345678"),
                "role_id" => "1",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],    
        );
    }
}
