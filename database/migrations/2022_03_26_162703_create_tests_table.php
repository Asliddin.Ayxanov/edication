<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->id();
            $table->string('savol');
            $table->string('a');
            $table->string('b');
            $table->string('c');
            $table->string('d');
            $table->unsignedBigInteger('lesson_id');
            $table->string('javob');
            $table->timestamps();

            $table->foreign("lesson_id")->references("id")->on("lessons")->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
