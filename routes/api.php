<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\LessonController;
use App\Http\Controllers\SubjectController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [LoginController::class, 'authenticate']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/logout', [UsersController::class,'Logout']);
Route::prefix('/user')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/auth_user',  [UsersController::class,'AuthUser']);
        Route::get('/get_users', [UsersController::class,'GetUsers']);
        Route::post('/create_user', [UsersController::class,'CreateUser']);
        Route::post('/update_user', [UsersController::class,'UpdateUser']);
        Route::post('/delete_user', [UsersController::class,'DeleteUser']);
        Route::post('/more_delete_user', [UsersController::class,'MoreDeleteUser']);
    });
Route::prefix('/role')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_roles', [RolesController::class,'GetRoles']);
        Route::post('/create_role', [RolesController::class,'CreateRole']);
        Route::post('/update_role', [RolesController::class,'UpdateRole']);
        Route::post('/delete_role', [RolesController::class,'DeleteRole']);
});
Route::prefix('/group')
    // ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/', [GroupController::class,'getGroup']);
        Route::post('/create_group', [GroupController::class,'CreateGroup']);
        Route::post('/update_group', [GroupController::class,'UpdateGroup']);
        Route::post('/delete_group', [GroupController::class,'DeleteGroup']);
        Route::post('/multidelete', [GroupController::class,'multidelete']);
});
Route::prefix('/subject')
    // ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/', [SubjectController::class,'getSubject']);
        Route::post('/create_subject', [SubjectController::class,'CreateSubject']);
        Route::post('/update_subject', [SubjectController::class,'UpdateSubject']);
        Route::post('/delete_subject', [SubjectController::class,'DeleteSubject']);
        Route::post('/multidelete', [SubjectController::class,'multidelete']);
});
Route::prefix('/lesson/{id}')
    // ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/', [LessonController::class,'getLesson']);
        Route::post('/create_lesson', [LessonController::class,'CreateLesson']);
        Route::post('/update_lesson', [LessonController::class,'Updatelesson']);
        Route::post('/delete_lesson', [LessonController::class,'Deletelesson']);
        Route::post('/multidelete', [LessonController::class,'multidelete']);
});